from sklearn import svm
import cv2
import numpy as np
import pickle
import time as tm
class multisvr:
    def Load_model(self):
        with open('Compressed/model.pickle', 'rb') as fc:                  #Retrieve the saved model
            self.clfs = pickle.load(fc)
        with open('Compressed/image_dim.pickle', 'rb') as fc:                 #Retrieve output dimensions
            self.dims = pickle.load(fc)
        with open('Compressed/wind_dim.pickle', 'rb') as fc:                 #Retrieve output dimensions
            self.wind_dims = pickle.load(fc)
        return

    def Predict(self, X_test):
        #print(self.clf)
        Y_pred = self.clf.predict(X_test)
        return Y_pred

st = tm.time()
r1 = multisvr()
r1.Load_model()
im_new = np.zeros((r1.dims))
wind_size = r1.wind_dims
xlims = np.arange(0, im_new.shape[1], wind_size)
ylims = np.arange(0, im_new.shape[0], wind_size)
c = 0
for i1 in range(1, len(xlims)):
    for j1 in range(1, len(ylims)):
        X_test = []
        for i in range(xlims[i1-1], xlims[i1]):
            for j in range(ylims[j1-1], ylims[j1]):
                X_test.append([j, i])
        X_test = np.array(X_test)
        Y_test = r1.clfs[c].predict(X_test)
        c += 1
        c1 = 0
        for i in range(xlims[i1-1], xlims[i1]):
            for j in range(ylims[j1-1], ylims[j1]):
                im_new[j][i] = Y_test[c1]
                c1 += 1
cv2.imwrite('Decompressed_Image.jpg', im_new)
print("Total Time: {:.2f}s".format(tm.time()-st))
"""
X_test = []
for i in range(r1.dims[0]):
    for j in range(r1.dims[1]):
        X_test.append([i, j])
X_test = np.array(X_test)
Y_test = r1.Predict(X_test)
c = 0
for i in range(r1.dims[0]):
    for j in range(r1.dims[1]):
        y_ = int(255 - 255*Y_test[c])
        im_new[i][j] = y_
        c += 1
#im_new = np.reshape(im_new, (im_new.shape[0], im_new.shape[1]))
"""