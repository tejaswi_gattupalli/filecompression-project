#New idea attempt
#1 SVR per Window slide through
from sklearn import svm
import cv2
import numpy as np
import pickle
import time as tm

class multisvr:
    def __init__(self):
        return

    def Fit(self, X_train, Y_train, img):
        self.clf = svm.LinearSVR(epsilon=0, tol=1e-2, C=1e-3, loss='epsilon_insensitive')
        self.clf.fit(X_train, Y_train)                  #Fit each feature of output separately
        return
    
    def Save_model(self, clfs, img, wind_size):
        with open('Compressed/model.pickle', 'wb') as fc:              #Save the model
            pickle.dump(clfs, fc)        
        with open('Compressed/image_dim.pickle', 'wb') as fc:             #Save the output dimensions
            pickle.dump(img.shape, fc)   
        with open('Compressed/wind_dim.pickle', 'wb') as fc:             #Save the output dimensions
            pickle.dump(wind_size, fc)
        return

st = tm.time()
fn = input("Enter file name: ")
fn1 = "Images/" + fn
img = cv2.imread(fn1, 0)
cv2.imwrite('Original_Image.jpg',img)
X_train,Y_train = [], []
wind_size = int(input("Enter window size:"))
#Scanning window by window
xlims = np.arange(0, img.shape[1], wind_size)
ylims = np.arange(0, img.shape[0], wind_size)
c = 0
clfs = []
for i1 in range(1, len(xlims)):
    for j1 in range(1, len(ylims)):
        #print("Window: {}".format(c))
        clf = svm.LinearSVR()
        #c += 1
        X_train, Y_train = [], []
        for i in range(xlims[i1-1], xlims[i1]):
            for j in range(ylims[j1-1], ylims[j1]):
                #print("{}, {} -> {}".format(i, j, img[i][j]))
                X_train.append([j, i])
                Y_train.append(img[j][i])
        X_train = np.array(X_train)
        Y_train = np.array(Y_train)
        clf.fit(X_train, Y_train)
        clfs.append(clf)
r2 = multisvr()
r2.Save_model(clfs, img, wind_size)

print("Total Time: {:.2f}s".format(tm.time()-st))
"""
for k in range(img.shape[0]*img.shape[1]):
    for i in range(wind_size):
        for j in range(wind_size):
            y = img[i][j]

            
for i in range(img.shape[0]):
    for j in range(img.shape[1]):
        y_ = (255.0-img[i][j])/255.0
        Y_train.append(y_)
        X_train.append([i, j])
X_train = np.array((X_train))
Y_train = np.array((Y_train))
r1 = multisvr()
r1.Fit(X_train, Y_train, img)
r1.Save_model(img)
""" 