from sklearn import svm
import cv2
import numpy as np
import pickle
import time as tm
class multisvr:
    
    def __init__(self):
        return

    def Load_model(self):
        with open('Compressed/model.pickle', 'rb') as fc:                  #Retrieve the saved model
            self.clf = pickle.load(fc)

        with open('Compressed/image_dim.pickle', 'rb') as fc:                 #Retrieve output dimensions
            self.dims = pickle.load(fc)
        return

    def Predict(self, X_test):
        #print(self.clf)
        Y_pred = self.clf.predict(X_test)
        return Y_pred

st = tm.time()
r1 = multisvr()
r1.Load_model()
im_new = np.zeros((r1.dims))
X_test = []
for i in range(r1.dims[0]):
    for j in range(r1.dims[1]):
        X_test.append([i, j])
X_test = np.array(X_test)
Y_test = r1.Predict(X_test)
c = 0
for i in range(r1.dims[0]):
    for j in range(r1.dims[1]):
        y_ = int(255 - 255*Y_test[c])
        im_new[i][j] = y_
        c += 1
#im_new = np.reshape(im_new, (im_new.shape[0], im_new.shape[1]))
cv2.imwrite('Decompressed_Image.jpg', im_new)
print("Total Time: {:.2f}s".format(tm.time()-st))