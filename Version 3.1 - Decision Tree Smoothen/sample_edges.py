import cv2
import numpy as np

fn = input("Enter file name: ")
fn1 = "Images/" + fn
img = cv2.imread(fn1, 0)
im = np.uint8(img)
ed = cv2.Canny(im, 0, 200)
i2 = np.array([[255 for x in r1] for r1 in img])
c = 0
for j in range(img.shape[1]):
    if(np.array_equal(ed[:,j], i2[:,j])):
        c += 1
print("{} edges".format(c))
cv2.imshow('Edges', ed)
cv2.imshow('zeros', i2)
cv2.waitKey()
cv2.destroyAllWindows()
