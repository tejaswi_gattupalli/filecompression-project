#New idea attempt Works for: a10.png
from sklearn.model_selection import cross_val_score                           #Library for evaluation of cross validation and scores
from sklearn.tree import DecisionTreeRegressor                             #Library for Decision trees
import cv2
import numpy as np
import pickle
import time as tm

class multisvr:
    def __init__(self):
        return

    def Fit(self, X_train, Y_train, img):
        self.clf = DecisionTreeRegressor(criterion="mse", random_state=None, max_features=None, max_depth=10, min_samples_leaf=4, min_samples_split=4, min_weight_fraction_leaf=5e-2)   
        self.clf.fit(X_train, Y_train)                  #Fit each feature of output separately
        return
    
    def Save_model(self, img):
        with open('Compressed/model.pickle', 'wb') as fc:              #Save the model
            pickle.dump(self.clf, fc)        
        with open('Compressed/image_dim.pickle', 'wb') as fc:             #Save the output dimensions
            pickle.dump(img.shape, fc)
        return


st = tm.time()
fn = input("Enter file name: ")
fn1 = "Images/" + fn
img = cv2.imread(fn1, 0)
cv2.imwrite('Original_Image.jpg',img)
X_train,Y_train = [], []
for i in range(img.shape[0]):
    for j in range(img.shape[1]):
        y_ = (255.0-img[i][j])/255.0
        Y_train.append(y_)
        X_train.append([i, j])

X_train = np.array((X_train))
Y_train = np.array((Y_train))
r1 = multisvr()
r1.Fit(X_train, Y_train, img)
r1.Save_model(img)
print("Total Time: {:.2f}s".format(tm.time()-st))
