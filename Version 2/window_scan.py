import cv2
import numpy as np
import time as tm
fn = input("Enter file name:")
img = cv2.imread("Images/"+fn, 0)
wind_size = int(input("Enter window size:"))
xlims = np.arange(0, img.shape[1], wind_size)
ylims = np.arange(0, img.shape[0], wind_size)
c = 0
for i1 in range(1, len(xlims)):
    for j1 in range(1, len(ylims)):
        print("Window: {}".format(c))
        c += 1
        for i in range(xlims[i1-1], xlims[i1]):
            for j in range(ylims[j1-1], ylims[j1]):
                print("{}, {} -> {}".format(i, j, img[i][j]))
    tm.sleep(3)