#New idea attempt
from sklearn import svm
import cv2
import numpy as np
import pickle
import time as tm

class multisvr:
    def __init__(self):
        return

    def Fit(self, X_train, Y_train, img):
        self.clf = svm.SVR(kernel='rbf', degree=3, gamma='auto', coef0=0.0, tol=1e-4, C=1e-4, epsilon=1e-2)
        self.clf.fit(X_train, Y_train)                  #Fit each feature of output separately
        return
    
    def Save_model(self, img):
        with open('Compressed/model.pickle', 'wb') as fc:              #Save the model
            pickle.dump(self.clf, fc)        
        with open('Compressed/image_dim.pickle', 'wb') as fc:             #Save the output dimensions
            pickle.dump(img.shape, fc)
        return


st = tm.time()
fn = input("Enter file name: ")
fn1 = "Images/" + fn
img = cv2.imread(fn1, 0)
cv2.imwrite('Original_Image.jpg',img)
X_train,Y_train = [], []
for i in range(img.shape[0]):
    for j in range(img.shape[1]):
        y_ = (255.0-img[i][j])/255.0
        Y_train.append(y_)
        i1=(img.shape[0]-i)/img.shape[0]
        j1=(img.shape[1]-j)/img.shape[1]
        X_train.append([i1, j1])

X_train = np.array((X_train))
Y_train = np.array((Y_train))
r1 = multisvr()
r1.Fit(X_train, Y_train, img)
r1.Save_model(img)
print("Total Time: {:.2f}s".format(tm.time()-st))

"""
#New idea attempt - Variable length group
#1 SVR per group of same pixel values
from sklearn import svm
import cv2
import numpy as np
import pickle
import time as tm

class Pickle:
    def Save_model(self, clfs, img):
        with open('Compressed/model.pickle', 'wb') as fc:              #Save the model
            pickle.dump(clfs, fc)        
        with open('Compressed/image_dim.pickle', 'wb') as fc:             #Save the output dimensions
            pickle.dump(img.shape, fc)   
        return

st = tm.time()
fn = input("Enter file name: ")
fn1 = "Images/" + fn 
img = cv2.imread(fn1, 0)
cv2.imwrite('Original_Image.jpg',img)
X_train = np.arange(256)
Y_train_x, Y_train_y = [], []
clfs = []
for k in range(256):
    X_k, Y_k = [], []
    for i in range(img.shape[0]):
        for j in range(img.shape[1]):
            if(img[i][j] == k):
                X_k.append(i), Y_k.append(j)
    Y_train_x.append(X_k)
    Y_train_y.append(Y_k)
c = 0
for xi,y1i,y2i in zip(X_train, Y_train_x, Y_train_y):
    print("{} -> {}, {}".format(xi, y1i, y2i))
r2 = Pickle()

for xi,y1i,y2i in zip(X_train, Y_train_x, Y_train_y):
    if(len(y1i)>0): 
        clf1 = svm.LinearSVR(epsilon=0, tol=1e-2, C=1e-3, loss='epsilon_insensitive')
        clf2 = svm.LinearSVR(epsilon=0, tol=1e-2, C=1e-3, loss='epsilon_insensitive')
        clf1.fit(xi, y1i)
        clf2.fit(xi, y2i)
        clfs.append(([clf1, clf2]))
r2.Save_model(clfs, img)
"""
