#Compare the original and retrieved images

import numpy as np
from math import log10
import os
import cv2
mse = 0.0
f1 = input("Enter original file name: ")
f2 = input("Enter retrieved file name: ")

orig_img = np.uint8(cv2.imread(f1))
dec_img = np.uint8(cv2.imread(f2))

#print(orig_img.shape)
#print(dec_img.shape)

for i in range(orig_img.shape[0]):
    for j in range(orig_img.shape[1]):
        for k in range(orig_img.shape[2]):
            mse += float((orig_img[i][j][k]-dec_img[i][j][k])**2)

rmse = mse**0.5

print("Root Mean Square Error(RMSE): {:.6f}".format(rmse))

psnr = 20*log10(255.0/rmse)

print("Peak Signal to Noise Ratio(PSNR): {:.6f}".format(psnr))

s1 = os.stat('Original_Image.jpg').st_size
s2 = os.stat('Compressed/image_dim.pickle').st_size+os.stat('Compressed/image_shp.pickle').st_size+os.stat('Compressed/model.pickle').st_size
print("Compression Ratio: {:.6f}".format(float(s1/s2)))