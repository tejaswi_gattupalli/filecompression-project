#Attempt to compress image
import cv2
import time as tm
import pickle
import numpy as np
def not_in(row, prev):
    if(len(prev) < 1):
        return 1
    for rw1 in prev:
        if((row==rw1).all()):
            return -1
        
    return 1

img = cv2.imread('ic1.png')
i = 0
c = 0
prev = []
print("Res: {}".format(img.shape))
tm.sleep(10)
print("Total number of pixels: {}".format(img.shape[0] * img.shape[1] * img.shape[2]))
for pl1 in img:
    for rw1 in pl1:
        if(not_in(rw1, prev) == 1):
            prev.append(rw1)
            if(c == 0):
                dict1 = {str(i): rw1}
            else:
                dict1[str(i)] = rw1
            c += 1
            print("Distinct pixel no: {}".format(c))
        i += 1
        
print("Number of distinct pixels: {}\nTotal number of pixels: {}".format(c, i))
print(prev)

#Compressed image
with open('comp_img.pkl','wb') as f:
    pickle.dump(dict1, f, pickle.HIGHEST_PROTOCOL)

#Reconstruct image
im_new = np.zeros((img.shape))
for i in range(im_new.shape[0]):
    for j in range(im_new.shape[1]):
        im_new[i][j] = dict1.get(str(i))

cv2.imshow('New_image', im_new)
        
