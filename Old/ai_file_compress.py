#New idea attempt
from sklearn import svm
import cv2
import numpy as np
import pickle
import time as tm

class multisvr:
    def __init__(self):
        return

    def Fit(self, X_train, Y_train, img):
        self.clfs = []                                      #Set of classifiers -> one per output feature
        dim = (int)(img.shape[1] * img.shape[2])
        for i in range(img.shape[0]):
            clf = svm.LinearSVR() #, gamma='auto', coef0=0.0, tol=1e-35, C=1e-3)
            clf.fit(X_train[i*dim:(i+1)*dim, 1:], Y_train[i*dim:(i+1)*dim])                  #Fit each feature of output separately
            self.clfs.append(clf)                           #Add into classifiers list
            print("\tFitting SVR for Channel -> {}\t{:.2f}s".format(i+1, tm.time()-st))
        return
    
    def Save_model(self, img):
        with open('model.pickle', 'wb') as fc:              #Save the model
            pickle.dump(self.clfs, fc)        
        with open('image_dim.pickle', 'wb') as fc:             #Save the output dimensions
            pickle.dump(img.shape, fc)
        return

st = tm.time()
fn = input("Enter file name: ")
img = cv2.imread(fn)
img = np.reshape(np.array(img), (3, img.shape[0], img.shape[1]))
X_train,Y_train = [], []
for i in range(img.shape[0]):
    for j in range(img.shape[1]):
        for k in range(img.shape[2]):
            y_ = (255.0-img[i][j][k])/255.0
            Y_train.append(y_)
            X_train.append([i,j,k])

X_train = np.array((X_train))
Y_train = np.array((Y_train))
r1 = multisvr()
r1.Fit(X_train, Y_train, img)
r1.Save_model(img)
