from numpy import random
from sklearn import svm
import numpy as np
X_train, Y_train = [], []
X0, X1, X2 = [], [], []
Y0, Y1, Y2 = [], [], []
for i in range(3):
    for l in range(2):
        x,y,z = random.randint(0, 100), random.randint(0, 100), random.randint(0, 100)
        curr = []
        curr.append(i)
        curr.append(x)
        curr.append(y)
        X_train.append(curr)
        Y_train.append(z)
        #X1.append([x, y])
        if(i == 0):
            X0.append([x, y])
            Y0.append(z)
        elif(i==1):
            X1.append([x, y])
            Y1.append(z)
        else:
            X2.append([x, y])
            Y2.append(z)

print(X_train)
print(Y_train)
clfs = []
for i in range(3):
    clf = svm.SVR()
    if(i == 0):
        clf.fit(X0, Y0)
    elif(i==1):
        clf.fit(X1, Y1)
    else:
        clf.fit(X2, Y2)
    clfs.append(clf)
i = 0
Y = []
for clf in clfs:
    if(i == 0):
        Y.append(clf.predict(X0))
    elif(i == 1):
        Y.append(clf.predict(X1))
    else:
        Y.append(clf.predict(X2))
    i += 1
#print(Y)
Y2 = []
for i in range(len(Y)):
    Y2.append(Y[i][0])
    Y2.append(Y[i][1])
print(Y2)
var = sum([(di - yi)**2 for di, yi in zip(Y2, Y_train)])
print("Variance: {}".format(var))