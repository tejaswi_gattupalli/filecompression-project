from sklearn import svm
import cv2
import numpy as np
import pickle
import time as tm
class multisvr:
    
    def __init__(self):
        return

    def Load_model(self):
        with open('model.pickle', 'rb') as fc:                  #Retrieve the saved model
            self.clfs = pickle.load(fc)

        with open('image_dim.pickle', 'rb') as fc:                 #Retrieve output dimensions
            self.dims = pickle.load(fc)
        return

    def Predict(self, X_test):
        Y_pred = np.zeros((X_test.shape[0]))
        dim = (int)(self.dims[1] * self.dims[2])
        for i in range(self.dims[0]):
            Y_pred[i*dim:(i+1)*dim] = self.clfs[i].predict(X_test[i*dim:(i+1)*dim, 1:])          #Predict each feature(column) of output
        return Y_pred

st = tm.time()
r1 = multisvr()
r1.Load_model()
im_new = np.zeros((r1.dims))
X_test = []
for i in range(r1.dims[0]):
    for j in range(r1.dims[1]):
        for k in range(r1.dims[2]):
            X_test.append([i,j,k])
X_test = np.array(X_test)
Y_test = r1.Predict(X_test)
c = 0
for i in range(r1.dims[0]):
    for j in range(r1.dims[1]):
        for k in range(r1.dims[2]):
            y_ = int(255 - 255*Y_test[c])
            l1 = y_
            if(l1<0 or l1>255):
                l1 = 127
            im_new[i][j][k] = l1
            c += 1
im_new = np.reshape(im_new, (im_new.shape[1], im_new.shape[2], 3))
cv2.imwrite('Decomp_img.jpg', im_new)