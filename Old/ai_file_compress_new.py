#New idea attempt 2 with MLPN
import tensorflow as tf
from sklearn import svm
import cv2
import numpy as np
import pickle
import time as tm
from math import exp
def Onehotvector(d1, n):    
    train_target = []
    for ele in d1:
        curr = []
        for j in range(n):
            if(j == ele):
                curr.append(1)
            else:
                curr.append(0)
        train_target.append(curr)    
    return np.array(train_target)

def Argmax(x):
    for i in range(len(x)):
        if(x[i] == 1):
            return i
        
def Activate(x):
    return 1.0/(1.0 + exp(-x))

def Diff(x):
    y = Activate(x)
    return y * (1 - y)

class MLP:
    
    def __init__(self, eta, num_epochs):
        self.eta = eta
        self.num_epochs = num_epochs
        return
        
    def Forwardprop(self, xi):
        for i in range(len(self.v1)):
            self.v1[i] = (np.dot(np.transpose(self.w1)[i], xi))
        for i in range(len(self.y1)):
            self.y1[i] = (Activate(self.v1[i])) 
        for i in range(len(self.v2)):
            self.v2[i] = (np.dot(np.transpose(self.w2)[i], self.y1)) 
        for i in range(len(self.y2)):
            self.y2[i] = ((Activate(self.v2[i])))
        return self.y2
        
    def Backwardprop(self, xi, di):
        self.errors = (di - self.y2)
        self.cost += sum([i**2 for i in self.errors])
        for i in range(len(self.y2)):
            self.G2[i] = (self.errors[i] * Diff(self.v2[i]))
        
        for i in range(len(self.y1)):
            s = 0
            for j in range(len(self.y2)):
                s += (self.G2[j] * self.w2[i][j])
            self.G1[i] = s * (Diff(self.v1[i]))
            
        for i in range(3):
            for j in range(len(self.y1)):
                self.w1[i][j] += (self.eta) * (self.G1[j]) * (xi[i])
        
        for i in range(len(self.y1)):
            for j in range(len(self.y2)):
                self.w2[i][j] += (self.eta) * (self.G2[j]) * (self.y1[i])
        return
    
    def Fit(self, tdata, ttarget):
        #Weights input -> hidden layer
        self.w1 = np.zeros((3, 10))
        #Weights hidden -> output layer
        self.w2 = np.zeros((10, 255))
        for i in range(3):
            for j in range(10):
                self.w1[i][j] = np.random.uniform(-0.5, 0.5)
                
        for i in range(10):
            for j in range(255):
                self.w2[i][j] = np.random.uniform(-0.5, 0.5)
        self.v1 = np.zeros((10))
        self.y1 = np.zeros((10))
        self.v2 = np.zeros((255))
        self.y2 = np.zeros((255))
        self.G1 = np.zeros(len(self.y1))
        self.G2 = np.zeros(len(self.y2))
        for epoch in range(self.num_epochs):
            self.cost = 0
            for xi, di in zip(tdata, ttarget):
                self.Forwardprop(xi)
                self.Backwardprop(xi, di)
            print("Epoch: {}\tCost: {}".format(epoch, (self.cost/tdata.shape[0])))
        return
        
    def Predict(self, x):
        for i in range(len(self.v1)):
            self.v1[i] = (np.dot(np.transpose(self.w1)[i], xi))
        for i in range(len(self.y1)):
            self.y1[i] = (Activate(self.v1[i])) 
        for i in range(len(self.v2)):
            self.v2[i] = (np.dot(np.transpose(self.w2)[i], self.y1)) 
        for i in range(len(self.y2)):
            self.y2[i] = (round(Activate(self.v2[i])))
        return argmax(self.y2)


def Image_read(fn):    
    img = cv2.imread(fn)
    img = np.reshape(np.array(img), (3, img.shape[0], img.shape[1]))
    X_train,Y_train = [], []
    for i in range(img.shape[0]):
        for j in range(img.shape[1]):
            for k in range(img.shape[2]):
                y_ = img[i][j][k] #(255.0-img[i][j][k])/255.0
                Y_train.append(y_)
                X_train.append([i,j,k])
    X_train = np.array((X_train))
    Y_train = np.array((Y_train))
    return [X_train, Y_train]

fn = input("Enter the file name: ")
X_train, Y_train = Image_read(fn)
Y_train = Onehotvector(Y_train, 255)
m1 = MLP(0.1, 1000)
m1.Fit(X_train, Y_train)

img = cv2.imread(fn)
print("Expected\tPredicted")
for i in range(3):
    for j in range(img.shape[0]):
        for k in range(img.shape[1]):
            print("{}\t{}".format(img[i][j][k], m1.Predict([i,j,k])))




        
        
        
                                       


          

